<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
 
namespace app\admin\validate;
use think\validate;

class Extend extends validate {


	protected $rule = [
		'title'=>['require'],
	];

	protected $message = [
		'title.require'=>'名称不能为空',
	];

	protected $scene  = [
		'add'=>['title'],
		'update'=>['title'],
	];



}

