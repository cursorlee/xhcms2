<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
namespace app\admin\controller;

use app\admin\service\MemberService;
use app\admin\model\Member as MemberModel;

class Member extends Admin {


	/*首页数据列表*/
	function index(){
		if (!$this->request->isAjax()){
			return view('index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where = [];
			$where['username'] = $this->request->param('username', '', 'serach_in');
			$where['sex'] = $this->request->param('sex', '', 'serach_in');
			$where['mobile'] = $this->request->param('mobile', '', 'serach_in');
			$where['email'] = $this->request->param('email', '', 'serach_in');

			$amount_start = $this->request->param('amount_start', '', 'serach_in');
			$amount_end = $this->request->param('amount_end', '', 'serach_in');

			$where['amount'] = ['between',[$amount_start,$amount_end]];
			$where['status'] = $this->request->param('status', '', 'serach_in');
			$where['province'] = $this->request->param('province', '', 'serach_in');
			$where['city'] = $this->request->param('city', '', 'serach_in');
			$where['district'] = $this->request->param('district', '', 'serach_in');
			$where['tag'] = $this->request->param('tag', '', 'serach_in');

			$create_time_start = $this->request->param('create_time_start', '', 'serach_in');
			$create_time_end = $this->request->param('create_time_end', '', 'serach_in');

			$where['create_time'] = ['between',[strtotime($create_time_start),strtotime($create_time_end)]];

			$order  = $this->request->post('order', '', 'serach_in');	//排序字段 bootstrap-table 传入
			$sort  = $this->request->post('sort', '', 'serach_in');		//排序方式 desc 或 asc

			$field = 'member_id,username,sex,mobile,email,headimg,amount,status,province,city,district,tag,create_time';
			$orderby = ($sort && $order) ? $sort.' '.$order : 'member_id desc';

			$res = MemberService::indexList(formatWhere($where),$field,$orderby,$limit,$page);
			return json($res);
		}
	}

	/*修改排序开关按钮操作*/
	function updateExt(){
		$postField = 'member_id,status';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(!$data['member_id']) $this->error('参数错误');
		try{
			MemberModel::update($data);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return view('add');
		}else{
			$postField = 'username,sex,mobile,email,headimg,amount,status,province,city,district,tag,create_time,password';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = MemberService::add($data);
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$member_id = $this->request->get('member_id','','serach_in');
			if(!$member_id) $this->error('参数错误');
			$this->view->assign('info',checkData(MemberModel::find($member_id)));
			return view('update');
		}else{
			$postField = 'member_id,username,sex,mobile,email,headimg,amount,status,province,city,district,tag,create_time';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = MemberService::update($data);
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('member_id', '', 'serach_in');
		if(!$idx) $this->error('参数错误');
		try{
			MemberModel::destroy(['member_id'=>explode(',',$idx)]);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看详情*/
	function view(){
		$member_id = $this->request->get('member_id','','serach_in');
		if(!$member_id) $this->error('参数错误');
		$this->view->assign('info',MemberModel::find($member_id));
		return view('view');
	}

	
	/*充值*/
	function recharge(){
		if (!$this->request->isPost()){
			$info['member_id'] = $this->request->get('member_id','','serach_in');
			return view('recharge',['info'=>$info]);
		}else{
			$postField = 'member_id,amount';
			$data = $this->request->only(explode(',',$postField),'post',null);
			if(empty($data['member_id'])) $this->error('参数错误');
			$res = MemberService::recharge(['member_id'=>explode(',',$data['member_id'])],$data);
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*回收*/
	function backrecharge(){
		if (!$this->request->isPost()){
			$info['member_id'] = $this->request->get('member_id','','serach_in');
			return view('backrecharge',['info'=>$info]);
		}else{
			$postField = 'member_id,amount';
			$data = $this->request->only(explode(',',$postField),'post',null);
			if(empty($data['member_id'])) $this->error('参数错误');
			$res = MemberService::backrecharge(['member_id'=>explode(',',$data['member_id'])],$data);
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*重置密码*/
	function uppass(){
		if (!$this->request->isPost()){
			$info['member_id'] = $this->request->get('member_id','','serach_in');
			return view('uppass',['info'=>$info]);
		}else{
			$postField = 'member_id,password';
			$data = $this->request->only(explode(',',$postField),'post',null);
			if(empty($data['member_id'])) $this->error('参数错误');
			MemberService::uppass($data);
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}



}

