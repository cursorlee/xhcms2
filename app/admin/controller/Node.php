<?php 
/*
 module:		node
 create_time:	2020-05-02 21:55:18
 author:		寒塘冷月
 contact:		qq：274363574
*/

namespace app\admin\controller;

use app\admin\service\NodeService;
use app\admin\model\Node as NodeModel;

class Node extends Admin {


	/*首页数据列表*/
	function index(){
		if (!$this->request->isAjax()){
			return view('index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where = [];

			$order  = $this->request->post('order', '', 'serach_in');	//排序字段 bootstrap-table 传入
			$sort  = $this->request->post('sort', '', 'serach_in');		//排序方式 desc 或 asc

			$field = '*';
			$orderby = ($sort && $order) ? $sort.' '.$order : 'sortid asc';

			$res = NodeService::indexList(formatWhere($where),$field,$orderby,$limit,$page);
			$res['rows'] = formartList(['id', 'pid', 'title','title'],$res['rows']);
			return json($res);
		}
	}

	/*修改排序开关按钮操作*/
	function updateExt(){
		$postField = 'id,sortid,status,is_menu';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(!$data['id']) $this->error('参数错误');
		try{
			NodeModel::update($data);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return view('add');
		}else{
			$postField = 'title,val,pid,sortid,status,is_menu,icon';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = NodeService::add($data);
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','serach_in');
			if(!$id) $this->error('参数错误');
			$this->view->assign('info',checkData(NodeModel::find($id)));
			return view('update');
		}else{
			$postField = 'id,title,val,pid,sortid,status,is_menu,icon';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = NodeService::update($data);
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('id', '', 'serach_in');
		if(!$idx) $this->error('参数错误');
		try{
			NodeModel::destroy(['id'=>explode(',',$idx)]);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看详情*/
	function view(){
		$id = $this->request->get('id','','serach_in');
		if(!$id) $this->error('参数错误');
		$this->view->assign('info',NodeModel::find($id));
		return view('view');
	}



}

